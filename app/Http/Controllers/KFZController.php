<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KFZ;
use Illuminate\Support\Facades\DB;

class KFZController extends Controller
{    
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $allData = KFZ::all();
        $filterKennzeichen = DB::select("SELECT DISTINCT(kennzeichen) FROM k_f_z_s ORDER BY kennzeichen ASC");
        $filterStadt = DB::select("SELECT DISTINCT(stadt) FROM k_f_z_s ORDER BY stadt ASC");
        $filterBundesland = DB::select("SELECT DISTINCT(bundesland) FROM k_f_z_s ORDER BY bundesland ASC");

        return view('kfz.list')->with('data', ['allData' => $allData, 'filterKennzeichen' => $filterKennzeichen, 'filterStadt' => $filterStadt, 'filterBundesland' => $filterBundesland]);
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $allData = KFZ::all();
        $filterKennzeichen = DB::select("SELECT DISTINCT(kennzeichen) FROM k_f_z_s ORDER BY kennzeichen ASC");
        $filterStadt = DB::select("SELECT DISTINCT(stadt) FROM k_f_z_s ORDER BY stadt ASC");
        $filterBundesland = DB::select("SELECT DISTINCT(bundesland) FROM k_f_z_s ORDER BY bundesland ASC");

        $formData  = $request->all();

        if (array_key_first($formData) != "") {            
            $query = "SELECT * FROM k_f_z_s WHERE ";
            $query = $query.array_key_first($formData)." IN (";
            $query = $query."'".str_replace(',', "','", implode(',', array_shift($formData)))."'".")";
            while (array_key_first($formData) != "") {
                $query = $query." OR ".array_key_first($formData)." IN (";
                $query = $query."'".str_replace(',', "','", implode(',', array_shift($formData)))."'".")";
            }
            $allData = DB::select($query);
        }      
        return view('kfz.search', ['allData' => $allData, 'filterKennzeichen' => $filterKennzeichen, 'filterStadt' => $filterStadt, 'filterBundesland' => $filterBundesland]);
    }
}
