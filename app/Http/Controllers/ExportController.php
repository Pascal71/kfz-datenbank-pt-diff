<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KFZ;
use XMLWriter;

class ExportController extends Controller
{
    public function exportCsv(Request $request)
    {
        $fileName = 'KFZ.csv';
        $data = KFZ::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('ID', 'Kennzeichen', 'Stadt', 'Bundesland');

        $callback = function() use($data, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($data as $item) {
                $row['ID'] = $item->id;
                $row['Kennzeichen'] = $item->kennzeichen;
                $row['Stadt'] = $item->stadt;
                $row['Bundesland'] = $item->bundesland;

                fputcsv($file, array($row['ID'], $row['Kennzeichen'], $row['Stadt'], $row['Bundesland']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function exportJSON(Request $request){
        $table = KFZ::all();
        $filename = "KFZ.json";
        $handle = fopen($filename, 'w+');
        fputs($handle, $table->toJson(JSON_PRETTY_PRINT));
        fclose($handle);
        $headers = array('Content-type'=> 'application/json');
        return response()->download($filename,'KFZ.json',$headers);
   }

   public function exportXML(Request $request)
   {
        $data = KFZ::all()->toArray();
        $filename = "KFZ.xml";

        $xml = new XMLWriter();
        $xml->openUri('KFZ.xml');
        $xml->startDocument('1.0');
        $xml->startElement('Kennzeichen-Datenbank');
        foreach ($data as $item) {
            $xml->startElement('Kennzeichen');
            $xml->writeElement('id', $item['id']);
            $xml->writeElement('kennzeichen', $item['kennzeichen']);
            $xml->writeElement('stadt', $item['stadt']);
            $xml->writeElement('bundesland', $item['bundesland']);
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();
        $xml->flush();
        
       return response()->download($filename, 'KFZ.xml', ['Content-Type: text/xml']);
   }
}
