<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KFZ;
use Illuminate\Support\Facades\DB;
use SimpleXMLElement;

class ImportController extends Controller
{
    public function getImportCSV()
    {
        return view('importCSV');
    }

    public function getImportJSON()
    {
        return view('importJSON');
    }

    public function getImportXML()
    {
        return view('importXML');
    }

    public function importCsv(Request $request)
    {
        DB::table('k_f_z_s')->delete();
        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        if ($request->has('header')) {
            array_shift($data);
        }
        foreach ($data as $row) {
            $kfzData = new KFZ();
            $kfzData['id'] = $row[0];
            $kfzData['kennzeichen'] = $row[1];
            $kfzData['stadt'] = $row[2];
            $kfzData['bundesland'] = $row[3];
            $kfzData->save();
        }
        return view('kfzWelcome');
    }

    public function importJSON(Request $request)
    {
        DB::table('k_f_z_s')->delete();
        $file = $request->file('json_file');
        $fileData = file_get_contents($file, true);
        $data = json_decode($fileData);
        foreach ($data as $obj) {
            $kfzData = new KFZ();
            $kfzData['id'] = $obj->id;
            $kfzData['kennzeichen'] = $obj->kennzeichen;
            $kfzData['stadt'] = $obj->stadt;
            $kfzData['bundesland'] = $obj->bundesland;
            $kfzData->save();
        }
        return view('kfzWelcome');
    }

    public function importXml(Request $request)
    {
        DB::table('k_f_z_s')->delete();
        $file = $request->file('xml_file');
        $fileData = file_get_contents($file);
        $xml = new SimpleXMLElement($fileData);
        foreach ($xml->Kennzeichen as $item) {
            $kfzData = new KFZ();
            $kfzData['id'] = $item->id;
            $kfzData['kennzeichen'] = $item->kennzeichen;
            $kfzData['stadt'] = $item->stadt;
            $kfzData['bundesland'] = $item->bundesland;
            $kfzData->save();
        }
        return view('kfzWelcome');
    }
}
