$(function() {

	$('select[multiple].active.kennzeichen').multiselect({
	  columns: 1,
	  placeholder: 'Kennzeichen',
	  search: true,
	  searchOptions: {
	      'default': 'Suche...'
	  },
	  selectAll: false
	});

	$('select[multiple].active.stadt').multiselect({
		columns: 1,
		placeholder: 'Stadt',
		search: true,
		searchOptions: {
			'default': 'Suche...'
		},
		selectAll: false
	});

	$('select[multiple].active.bundesland').multiselect({
		columns: 1,
		placeholder: 'Bundesland',
		search: true,
		searchOptions: {
			'default': 'Suche...'
		},
		selectAll: false
	});

});