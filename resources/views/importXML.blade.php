@extends('kfzPlain')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <h3>XML Import</h3>
                    <br>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import_xml') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('xml_file') ? ' has-error' : '' }}">
                                <label for="xml_file" class="col-md-4 control-label">XML file to import</label>

                                <div class="col">
                                    <input id="xml_file" type="file" class="form-control" name="xml_file" required>

                                    @if ($errors->has('xml_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('xml_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Parse XML
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection