@extends('kfzPlain')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <h3>JSON Import</h3>
                    <br>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import_json') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('json_file') ? ' has-error' : '' }}">
                                <label for="json_file" class="col-md-4 control-label">JSON file to import</label>

                                <div class="col">
                                    <input id="json_file" type="file" class="form-control" name="json_file" required>

                                    @if ($errors->has('json_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('json_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Parse JSON
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection