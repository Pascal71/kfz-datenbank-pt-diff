@extends('kfzPlain')
@section('content')

<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<script src="../js/jquery.multiselect.js"></script>
<script src="../js/main.js"></script>

<table id="list" class="table table-striped table-bordered">
    <h1><caption-top>KFZ List</caption-top></h1>
    <form method="GET" action="{{ route('search') }}">
        <div class="container">
            <div class="row justify-content-evenly">
                <div class="col-4">
                    <select name="kennzeichen[]" class="kennzeichen active form-control" multiple>
                        @foreach($filterKennzeichen as $item)
                            <option value="{{ $item->kennzeichen }}">{{ $item->kennzeichen }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <select name="stadt[]" class="stadt active form-control" multiple>
                        @foreach($filterStadt as $item)
                            <option value="{{ $item->stadt }}">{{ $item->stadt }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <select name="bundesland[]" class="bundesland active form-control" multiple>
                        @foreach($filterBundesland as $item)
                            <option value="{{ $item->bundesland }}">{{ $item->bundesland }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-3">
            <div class="form-group">
                <button id="search" class="btn btn-outline-success" type="submit">Search</button>
            </div>
        </div>
        <br>
    </form>
    <thead>
        <tr class="table-info">
            <th class="pointer">Kennzeichen</th>
            <th class="pointer">Stadt</th>
            <th class="pointer">Bundesland</th>
            <th>Wikipedia</th>
            <th>Google Maps</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($allData as $item)
        <tr>
            <td>{{ $item->kennzeichen }}</td>
            <td>{{ $item->stadt }}</td>
            <td>{{ $item->bundesland }}</td>
            <td style="text-align: center">
                <a href="<?php
                    $wikiURL = "https://de.wikipedia.org/wiki/";
                    $stadt = $item->stadt;
                    $URL = $wikiURL.$stadt;
                    echo $URL
                ?>" target="_blank"><img alt="Wikipedia" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/32px-Wikipedia-logo-v2.svg.png"></a>
            </td>
            <td style="text-align: center">
                <a href="<?php
                    $mapsURL = "https://www.google.com/maps/search/?api=1&query=";
                    $stadt = $item->stadt;
                    $stadt = str_replace(" ","+",$stadt);
                    $URL = $mapsURL.$stadt;
                    echo $URL
                ?>" target="_blank"><img alt="Google Maps" src="https://image.flaticon.com/icons/png/32/2642/2642502.png"></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

    const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
        v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
        )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

    // do the work...
    document.querySelectorAll('th.pointer').forEach(th => th.addEventListener('click', (() => {
        const table = th.closest('table');
        const tbody = table.querySelector('tbody');
        Array.from(tbody.querySelectorAll('tr'))
        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
        .forEach(tr => tbody.appendChild(tr));
        
        // Pfeile (Order Dircetion) entfernen
        Array.from(th.parentNode.children).forEach(element => element.classList.remove('headerSortUp', 'headerSortDown'));
        // Pfeil (Order Dircetion) beim entsprechender <th> hinzufügen
        node = th.parentNode.children[Array.from(th.parentNode.children).indexOf(th)];
        this.asc ? node.classList.add('headerSortDown') : node.classList.add('headerSortUp');
    })));
</script>

@endsection