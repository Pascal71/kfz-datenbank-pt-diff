@extends('kfzPlain')
@section('content')

<h1 class="center">Herzlich Willkommen</h1>
<br>
<p>Die Kennzeichenliste ist unter dem Menüpunkt "KFZ List" zu finden</p>
<br><br>
<p>Hier sind die verscheidenen Import und Export Möglichkeiten aufgeführt:</p>
<br><br>

<div class="container">
    <div class="row">
      <div class="col">Import</div>
      <div class="col">
        <form action="{{ route('importCSV') }}" method="get">
          <button type="submit" class="btn btn-success btn-sm">Import CSV</button>
        </form>
      </div>
      <div class="col">
        <form action="{{ route('importXML') }}" method="get">
          <button type="submit" class="btn btn-success btn-sm">Import XML</button>
        </form>
      </div>
      <div class="col">
        <form action="{{ route('importJSON') }}" method="get">
          <button type="submit" class="btn btn-success btn-sm">Import JSON</button>
        </form>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col">Export</div>
      <div class="col">
        <span data-href="/export-csv" id="csvExport" class="btn btn-success btn-sm" onclick="exportData(event.target);">Export CSV</span>
      </div>
      <div class="col">
        <span data-href="/export-xml" id="xmlExport" class="btn btn-success btn-sm" onclick="exportData(event.target);">Export XML</span>
      </div>
      <div class="col">
        <span data-href="/export-json" id="jsonExport" class="btn btn-success btn-sm" onclick="exportData(event.target);">Export JSON</span>
      </div>
    </div>
  </div>

<script>
    function exportData(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>

@endsection