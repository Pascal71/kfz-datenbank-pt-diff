<?php

use App\Http\Controllers\ExportController;
use App\Http\Controllers\ImportController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KFZController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('kfzWelcome');
})->name('home');

Route::get('KFZ/search', [KFZController::class, 'search'])->name('search');
Route::resource('KFZ', KFZController::class);


Route::get('/export-csv', [ExportController::class, 'exportCsv']);
Route::get('/export-json', [ExportController::class, 'exportJSON']);
Route::get('/export-xml', [ExportController::class, 'exportXML']);


Route::get('/importCSV', [ImportController::class, 'getImportCSV'])->name('importCSV');
Route::post('/import_csv', [ImportController::class, 'importCsv'])->name('import_csv');

Route::get('/importJSON', [ImportController::class, 'getImportJSON'])->name('importJSON');
Route::post('/import_json', [ImportController::class, 'importJson'])->name('import_json');

Route::get('/importXML', [ImportController::class, 'getImportXML'])->name('importXML');
Route::post('/import_xml', [ImportController::class, 'importXml'])->name('import_xml');